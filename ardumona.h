/*
 Copyright (C) 2018 H Tnunay <htnunay@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 3 as published by the Free Software Foundation.
 */

/**
 * @file ardumona.h
 *
 * Class declaration of Ardumona.
 */

#ifndef __ARDUMONA_H__
#define __ARDUMONA_H__

#include "ardumona_config.h"

void callback_left_encoder_interrupt(void);
void callback_right_encoder_interrupt(void);

class Ardumona{
	m_velocity unicycle_cmd;
	m_wheel desired_cmd;

	long control_start_time;

	m_PID_param motor_pid;
    m_PID_signal left_motor_signal;
    m_PID_signal right_motor_signal;

public:
	/**
	* ARDUMONA CONSTRUCTORS
	* Remark:
	* 1. Ardumona(bool, bool, bool, bool)
	*	 -Input:
	* 	 	::Enabling/disabling signal of serial communication
	*		::Enabling/disabling signal of motors
	*		::Enabling/disabling signal of infrared proximity sensors
	*		::Enabling/disabling signal of encoders	
	*/
	Ardumona();
	Ardumona(bool, bool, bool, bool);

	/**
	* GLOBAL INITIALISATION
	* Remark:
	* 1. initialisation(bool, bool, bool, bool)
	*	 -Input:
	* 	 	::Enabling/disabling signal of serial communication
	*		::Enabling/disabling signal of motors
	*		::Enabling/disabling signal of infrared proximity sensors
	*		::Enabling/disabling signal of encoders	
	*/
	bool initialisation(bool, bool, bool, bool);

	/**
	* SERIAL COMMUNICATION ROUTINES
	* Remark:
	* 1. send_serial(bool, bool, bool)
	*	 -Input:
	* 	 	::Enabling/disabling motor data serial transmission
	*		::Enabling/disabling IR data serial transmission
	*		::Enabling/disabling encoder data serial transmission
	*/
	bool initialise_serial(void);
	void send_serial(bool, bool, bool); //motor, IR, encoder
	
	/**
	* MOTOR-DRIVING ROUTINES
	* Remark:
	* 1. drive_motor_{left,right}(unsigned int, unsigned char)
	*	 -Usage: sending driving command to {left,right} motor without considering the PWM offset value.
	*	 -Input:
	*		::Rotation direction
	*		::PWM signal
	* 2. drive_motor_{left,right}_with_offset(unsigned int, unsigned char)
	*	 -Usage: sending driving command to {left,right} motor considering the PWM offset value.
	*	 -Input:
	*		::Rotation direction
	*		::PWM signal
	* 3. drive_motor(m_signal)
	*	 -Usage: sending driving command to both left and right motors at once.
	*	 -Input:
	*		::Struct of driving signals for both motors
	* 4. drive_motor_with_offset(m_signal)
	*	 -Usage: sending driving command to both left and right motors at once considering
	*			 the PWM offset value.
	*	 -Input:
	*		::Struct of driving signals for both motors
	* 5. convert_wheel_to_signal(m_wheel)
	*	 -Usage: converting data from m_wheel structure to m_signal structure, which are ready
	*			 to send to the motors.
	*/
	bool initialise_motor(void);
	bool drive_motor_left(unsigned int, unsigned char);
	bool drive_motor_left_with_offset(unsigned int, unsigned char);
	bool drive_motor_right(unsigned int, unsigned char);
	bool drive_motor_right_with_offset(unsigned int, unsigned char);
	bool drive_motor(m_signal);
	bool drive_motor_with_offset(m_signal);
	m_signal convert_wheel_to_signal(m_wheel);

	/**
	* ENCODER COMMAND ROUTINES
	* Remark:
	* 1. get_{left,right}_encoder_ticks(void)
	*	 -Usage: obtaining the raw encoder ticks
	* 2. get_{left,right}_time_period(void)
	*	 -Usage: obtaining the time period of the encoder sampling
	* 3. get_{left,right}_odometry(void)
	*	 -Usage: obtaining the odometry data
	* 4. get_{left,right}_motor_speed(void)
	*	 -Usage: obtaining the motor speed
	*/
	bool enable_encoder(void);
	bool reset_encoder_ticks(bool, bool);

	long get_left_encoder_ticks(void);
	double get_left_time_period(void);
	double get_left_odometry(void);
	long get_left_motor_speed(void);
	
	long get_right_encoder_ticks(void);
	double get_right_time_period(void);
	double get_right_odometry(void);
	long get_right_motor_speed(void);
	
	/**
	* INFRARED COMMAND ROUTINES
	* Remark:
	* 1. get_IR_data(void)
	*	 -Usage: obtaining all of the IR sensor data
	*/
	bool enable_IR(void);
	bool disable_IR(void);
	m_IR_sensor get_IR_data(void);

	/**
	* BATTERY COMMAND ROUTINES
	* Remark:
	* 1. get_battery_data(void)
	*	 -Usage: obtaining the battery measurement data
	*/
	void get_battery_data(void);


	/**
	* CONTROL COMMAND ROUTINES
	* Remark:
	* 1. update_desired_control_input(void)
	*	 -Usage: assigning the serial-received control commands to the desired control inputs
	* 2. update_desired_motor_speed(void)
	* 	 -Usage: applying the desired control inputs to the differential drive dynamics	
	* 3. update_control_motor_pwm(long, long)
	*	 -Usage: applying the desired speed of each motor to the low level controller
	*	 -Input: 
	*		::desired speed of left motor
	*		::desired speed of right motor
	* 4. update_controller(void)
	*	 -Usage: combining the sequences of controller from serial inputs to applying the command to motors
	* 5. update_direct_motor_controller(void)
	*	 -Usage: this routine is only for testing the low-level controller, directly assign the serial input
	*			 to the desired speed of each motor
	* 6. update_PID(m_PID_param, m_PID_signal)
	*	 -Usage: it is a general function to calculate the PID control signal for a given input.
	*	 -Input: 
	*		::struct of controller gains (kp, ki, kd)
	*		::struct of previous signals
	*/
	bool update_desired_control_input(void);
	bool update_desired_motor_speed(void);	
	bool update_control_motor_pwm_pid(long, long);
	bool update_controller(void);
	bool update_direct_motor_controller(void);

	m_PID_signal update_PID(m_PID_param, m_PID_signal);
};

#endif