/*
 Copyright (C) 2018 H Tnunay <htnunay@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 3 as published by the Free Software Foundation.
 */

/**
 * @file ardumona_config.h
 *
 * General declaration of Ardumona configuration.
 */

#ifndef __ARDUMONA_CONFIG_H__
#define __ARDUMONA_CONFIG_H__


/**
* DECLARATION OF USING ARDUINO
*/
#if (ARDUINO >= 100)
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif


/**
* MODE DEFINITION
*/
#define IMPLEMENTATION
// #define LOW_CONTROL_TEST


/**
* PHYSICAL-PARAMETER RELATED DEFINITION
*/
#define CONST_PI 3.14159265358979323846264f

#define PHYS_DIST_WHEEL2WHEEL 7.5f // mm
#define PHYS_RADIUS_WHEEL 1.6f //cm
#define PHYS_VELO_DIVIDER 1

#define PHYS_MAX_LINEAR_VELO 2500
#define PHYS_MIN_LINEAR_VELO -2500
#define PHYS_MAX_ANGULAR_VELO 650
#define PHYS_MIN_ANGULAR_VELO -650


/**
* SERIAL-COMMUNICATION-RELATED DEFINITION
*/
#define SERIAL_ENABLE 1
#define SERIAL_DISABLE 0

#define SERIAL_BAUDRATE 38400


/**
* LED-RELATED DEFINITION
*/
#define PIN_LED_TOP 13
#define PIN_LED_BOTTOM 12


/**
* MOTOR-RELATED DEFINITION
*/
#define MOTOR_ENABLE 1
#define MOTOR_DISABLE 0

#define PIN_MOTOR_LEFT_PWM_FORWARD 6
#define PIN_MOTOR_RIGHT_PWM_FORWARD 5
#define PIN_MOTOR_LEFT_DIR_FORWARD 9
#define PIN_MOTOR_RIGHT_DIR_FORWARD 10

#define PIN_MOTOR_LEFT_PWM_BACKWARD 9
#define PIN_MOTOR_RIGHT_PWM_BACKWARD 10
#define PIN_MOTOR_LEFT_DIR_BACKWARD 6
#define PIN_MOTOR_RIGHT_DIR_BACKWARD 5

#define MOTOR_PWM_OFFSET_FORWARD 14
#define MOTOR_PWM_OFFSET_BACKWARD 17

#define MOTOR_MAX_PWM 250
#define MOTOR_MIN_PWM 0

#define MOTOR_MAX_SPEED 1600
#define MOTOR_MIN_SPEED -1600


/**
* INFRARED-RELATED DEFINITION
*/
#define IR_ENABLE 1
#define IR_DISABLE 0

#define PIN_IR_ENABLE 4
#define PIN_IR_SIGNAL_RIGHT A7
#define PIN_IR_SIGNAL_FRONTRIGHT A0
#define PIN_IR_SIGNAL_FRONT A1
#define PIN_IR_SIGNAL_FRONTLEFT A2
#define PIN_IR_SIGNAL_LEFT A3


/**
* ENCODER-RELATED DEFINITION
* Remark:
* 1. Value of ENCODER_DISTANCE_PER_TICK is obtained using:
* (wheel_circumference/ticks_per_revolution)
*/
#define ENCODER_ENABLE 1
#define ENCODER_DISABLE 0

#define PIN_LEFT_ENCODER 2
#define PIN_RIGHT_ENCODER 3

// #define ENCODER_TICK_PER_REVOLUTION 250
#define ENCODER_TICK_PER_REVOLUTION 10

#define ENCODER_DISTANCE_PER_TICK 0.0009387755f


/**
* MOVEMENT-RELATED DEFINITION
*/
#define MOV_FORWARD 0
#define MOV_BACKWARD 1
#define MOV_LEFT 2
#define MOV_RIGHT 3
#define MOV_STOP 0


/**
* DRIVING-SIGNAL STRUCTURES
*/
struct m_signal{
	unsigned int left_dir;
	unsigned char left_pwm;
	unsigned int right_dir;
	unsigned char right_pwm;	
};

struct m_wheel{
	long left_speed;
	long right_speed;
};

struct m_velocity{
	int linear_velo;
	int angular_velo;	
};

struct m_velocity_f{
	float linear_velo;
	float angular_velo;
};

struct m_IR_sensor{
	int right;
	int front_right;
	int front;
	int front_left;
	int left;
};

struct m_PID_param{
	long kp;
	long ki;
	long kd;
	long divider;
};

struct m_PID_signal{
	long dt;
	long desired_value;
	long actual_value;
	long current_error;	
	long previous_error;
	long sum_error;
	long control_input;
};

#endif