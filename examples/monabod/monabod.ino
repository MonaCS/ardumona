#include <ardumona.h>

Ardumona monabot;

bool flag;
long le_encdr;
long ri_encdr;

void setup() {
  flag = monabot.initialisation(SERIAL_ENABLE, MOTOR_ENABLE, ENCODER_ENABLE, IR_ENABLE);
  Serial.println(flag);
}

void loop() { 
//  monabot.drive_motor_left_with_offset(MOV_FORWARD, 82);
//  monabot.drive_motor_right_with_offset(MOV_FORWARD, 82);
//  monabot.update_control_motor_pwm(130, 130);

  monabot.update_controller();
  monabot.send_serial(MOTOR_ENABLE, ENCODER_ENABLE, IR_DISABLE);
//  monabot.send_serial(DISABLE_MOTOR, DISABLE_ENCODER, DISABLE_IR);
}
