## Ardumona: An Arduino-Monabot library
This package is developed for the lower-level control library of the base of Mona robot which has been initiated by and documented in [MonaRobot](https://github.com/MonaRobot) repository and [bird43's MonaMk2](https://github.com/bird43/MonaMk2) repository.

### File System
There are two *.h* header files and a *.cpp* file and one main *.ino* file. These are the details:
1. **monabod.ino** : this is the main file defining the serial communication procedure, and updating the control value.
2. **ardumona_config.h** : this contains the configuration of the Mona Robot, including pins, boundary limit of some quantities, and some structured variables.
3. **ardumona.h** : this contains the class file declaration of ardumona library; there are private and public variables required for communication and low-level control.
4. **ardumona.cpp** : this contains the details of the class declared in **ardumona.h**, including the constructors and functions.

### Installation
1. Download and install Arduino IDE on your computer.
2. Download or clone [this repository](https://gitlab.com/MonaCS/ardumona) into a local folder on your computer. Some cases might require this repository to be downloaded or cloned into directory of the Arduino's library on your computer
3. Open the *monabod.ino* inside the *examples* directory and then compile it.
4. Connect the Mona Robot to the computer using micro USB cable.
5. Switch the power switch to the *programming* or *on* mode.
6. On the Arduino IDE, choose *Arduino Mini* or *Arduino Pro Mini* as the Board.
7. Compile and upload the program to the robot.

### Operation procedures
1. Make sure that the battery has enough power to supply the Mona Robot.
2. Switch the power switch to the *on* mode.
3. For operation, unplug the micro-USB cable from the computer. Or, for debugging purpose, keep the micro-USB cable plugged to the computer.

### Default Communication Protocol
The main communication is implemented using Serial. These are the default configurations:
1. Baudrate: **38400**
2. The format of the serial input is: 
	```
	L<linear_velocity>$A<angular_velocity>$
	```
	Note that, to avoid unstable system, the linear and angular velocty are bounded to be -2500<=linear_velocity<=2500 and -650<=angular_velocity<=650.
3. The format of the serial output is given by:
	```
	word = &
	if enable_send_motor_data, then
		word = word + #M#<motor_left_dir>#<motor_left_pwm#<motor_right_dir>#<motor_right_pwm>
	if enable_send_encoder_data, then
		word = word + #E#<left_motor_speed>#<right_motor_speed>
	if enable_send_IR_data, then
		word = word + #I#<IR_signal_right>#<IR_signal_front_right>#<IR_signal_front>#<IR_signal_front_left>#<IR_signal_left>#
	```
	While waiting until the full documentation is ready, please refer the variables, functions, etc directly to the code.

### Developers
1. Hilton Tnunay (htnunay@gmail.com)
2. Zhenhong Li