/*
 Copyright (C) 2018 H Tnunay <htnunay@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 3 as published by the Free Software Foundation.
 */

/**
 * @file ardumona.cpp
 *
 * Nested declaration of Ardumona.
 */

#include "ardumona.h"
#include <stdio.h>



int left_encoder_counter = 0;
int right_encoder_counter = 0;
volatile long left_encoder_ticks = 0;
volatile long right_encoder_ticks = 0;
long left_encoder_time_start = 0;
long right_encoder_time_start = 0;
long left_encoder_time_delta = 0;
long right_encoder_time_delta = 0;
long left_odometry_ticks = 0;
long right_odometry_ticks = 0;

bool left_motor_flag; // Direction of left motor
bool right_motor_flag; // Direction of right motor

m_wheel motor;  // Unsigned motor linear speed mesasured from encoder
m_signal driver; // Unsigned direction and PWM signal of left and right motors
m_velocity command_from_pi; // Measured motor rotation speed 

/**
* ARDUMONA CONSTRUCTORS
* Remark:
* 1. Ardumona(bool, bool, bool, bool)
*	 -Input:
* 	 	::Enabling/disabling signal of serial communication
*		::Enabling/disabling signal of motors
*		::Enabling/disabling signal of infrared proximity sensors
*		::Enabling/disabling signal of encoders	
*/
Ardumona::Ardumona(){
}
Ardumona::Ardumona(bool _enable_serial, bool _enable_motor, bool _enable_encoder, bool _enable_IR){
    if(_enable_serial){
        this->initialise_serial();
    }
    if(_enable_motor){
        this->initialise_motor();
    }
    if(_enable_IR){
        this->enable_IR();
    }
    if(_enable_encoder){
        this->enable_encoder();
    }
}


/**
* GLOBAL INITIALISATION
* Remark:
* 1. initialisation(bool, bool, bool, bool)
*	 -Input:
* 	 	::Enabling/disabling signal of serial communication
*		::Enabling/disabling signal of motors
*		::Enabling/disabling signal of infrared proximity sensors
*		::Enabling/disabling signal of encoders	
*/
bool Ardumona::initialisation(bool _enable_serial, bool _enable_motor, bool _enable_encoder, bool _enable_IR){
    bool init_flag = true;
    if(_enable_serial){
        init_flag &= this->initialise_serial();        
    }
    if(_enable_motor){
        init_flag &= this->initialise_motor();
    }
    if(_enable_IR){
        init_flag &= this->enable_IR();
    }
    if(_enable_encoder){
        init_flag &= this->enable_encoder();
    }

    this->left_motor_signal.control_input = 0;
    this->right_motor_signal.control_input = 0;

    this->control_start_time = 0;

    return init_flag;
}


/**
* SERIAL COMMUNICATION ROUTINES
* Remark:
* 1. send_serial(bool, bool, bool)
*	 -Input:
* 	 	::Enabling/disabling motor data serial transmission
*		::Enabling/disabling IR data serial transmission
*		::Enabling/disabling encoder data serial transmission
*/
bool Ardumona::initialise_serial(void){
    Serial.begin(SERIAL_BAUDRATE);
    Serial.setTimeout(10);
    Serial.println("Serial OK");
    return true;
}
void Ardumona::send_serial(bool _send_motor_data, bool _send_encoder_data, bool _send_IR_data){
    char words[200];
    sprintf(words, "&");
    if(_send_motor_data){
        sprintf(words, "%s#M#%d#%d#%d#%d", words, driver.left_dir, driver.left_pwm, driver.right_dir, driver.right_pwm);
    }    
    if(_send_encoder_data){        
        sprintf(words, "%s#E#%d", words, this->get_left_motor_speed());
        sprintf(words, "%s#%d", words, this->get_right_motor_speed());
        sprintf(words, "%s#E#%d", words, left_encoder_ticks);
        sprintf(words, "%s#%d", words, right_encoder_ticks);
    }
    if(_send_IR_data){
        m_IR_sensor IR_signal;
        IR_signal = this->get_IR_data();
        sprintf(words, "%s#I#%d#%d#%d#%d#%d", words, IR_signal.right, IR_signal.front_right, IR_signal.front, IR_signal.front_left, IR_signal.left);
    }
    sprintf(words, "%s#", words);
    Serial.println(words);
}
void serialEvent(){    
    String in_data = "";
    String linear_velo_read = "";
    String angular_velo_read = "";
    
    if (Serial.available()) {
        in_data = Serial.readString();
//	Serial.println(in_data);
        if(in_data.charAt(0) == '$' & in_data.charAt(13) == '$'){
            linear_velo_read = in_data.substring(2,7);
            command_from_pi.linear_velo = linear_velo_read.toInt(); // mm/s
            angular_velo_read = in_data.substring(9,13);
            command_from_pi.angular_velo = angular_velo_read.toInt(); // mrad/s
        }
    }
     //Serial.println(in_data.charAt(0));
     //Serial.println(Serial.available());
     //Serial.println(command_from_pi.linear_velo);
     //Serial.println(command_from_pi.angular_velo);
}


/**
* MOTOR-DRIVING ROUTINES
* Remark:
* 1. drive_motor_{left,right}(unsigned int, unsigned char)
*	 -Usage: sending driving command to {left,right} motor without considering the PWM offset value.
*	 -Input:
*		::Rotation direction
*		::PWM signal
* 2. drive_motor_{left,right}_with_offset(unsigned int, unsigned char)
*	 -Usage: sending driving command to {left,right} motor considering the PWM offset value.
*	 -Input:
*		::Rotation direction
*		::PWM signal
* 3. drive_motor(m_signal)
*	 -Usage: sending driving command to both left and right motors at once.
*	 -Input:
*		::Struct of driving signals for both motors
* 4. drive_motor_with_offset(m_signal)
*	 -Usage: sending driving command to both left and right motors at once considering
*			 the PWM offset value.
*	 -Input:
*		::Struct of driving signals for both motors
* 5. convert_wheel_to_signal(m_wheel)
*	 -Usage: converting data from m_wheel structure to m_signal structure, which are ready
*			 to send to the motors. (note that functions 1 2 3 4 only accept unsigned avariable, 
*            function 5 is designed to transfer signed PWM signal generated from controller to the drive signal of motors.
*/
bool Ardumona::initialise_motor(void){
    pinMode(PIN_MOTOR_LEFT_DIR_FORWARD, OUTPUT);
    pinMode(PIN_MOTOR_RIGHT_DIR_FORWARD, OUTPUT);
    pinMode(PIN_MOTOR_LEFT_PWM_FORWARD, OUTPUT);
    pinMode(PIN_MOTOR_RIGHT_PWM_FORWARD, OUTPUT);
        
    digitalWrite(PIN_MOTOR_LEFT_DIR_FORWARD, HIGH);
    digitalWrite(PIN_MOTOR_RIGHT_DIR_FORWARD, HIGH);
    digitalWrite(PIN_MOTOR_LEFT_PWM_FORWARD, ~MOTOR_MIN_PWM);
    digitalWrite(PIN_MOTOR_RIGHT_PWM_FORWARD, ~MOTOR_MIN_PWM);
    return true;
}
bool Ardumona::drive_motor_left(unsigned int _dir, unsigned char _PWM){
    driver.left_dir = _dir;
    driver.left_pwm = _PWM;
    
    left_motor_flag = _dir;
    if(_dir == MOV_FORWARD){            
        digitalWrite(PIN_MOTOR_LEFT_DIR_FORWARD, _dir+1);
        analogWrite(PIN_MOTOR_LEFT_PWM_FORWARD, ~_PWM);
    }else{        
        digitalWrite(PIN_MOTOR_LEFT_DIR_BACKWARD, _dir);
        analogWrite(PIN_MOTOR_LEFT_PWM_BACKWARD, ~_PWM);    
    }
    return true;
}
bool Ardumona::drive_motor_left_with_offset(unsigned int _dir, unsigned char _PWM){
    driver.left_dir = _dir;
    driver.left_pwm = _PWM;
    
    left_motor_flag = _dir;
    if(_dir == MOV_FORWARD){    
        digitalWrite(PIN_MOTOR_LEFT_DIR_FORWARD, _dir+1);
        analogWrite(PIN_MOTOR_LEFT_PWM_FORWARD, ~min(max(_PWM + MOTOR_PWM_OFFSET_FORWARD, MOTOR_MIN_PWM), MOTOR_MAX_PWM));
    }else{        
        digitalWrite(PIN_MOTOR_LEFT_DIR_BACKWARD, _dir);
        analogWrite(PIN_MOTOR_LEFT_PWM_BACKWARD, ~min(max(_PWM + MOTOR_PWM_OFFSET_BACKWARD, MOTOR_MIN_PWM), MOTOR_MAX_PWM));
    }
    return true;
}
bool Ardumona::drive_motor_right(unsigned int _dir, unsigned char _PWM){
    driver.right_dir = _dir;
    driver.right_pwm = _PWM;
            
    right_motor_flag = _dir;
    if(_dir == MOV_FORWARD){
        digitalWrite(PIN_MOTOR_RIGHT_DIR_FORWARD, _dir+1);
        analogWrite(PIN_MOTOR_RIGHT_PWM_FORWARD, ~_PWM);
    }else{        
        digitalWrite(PIN_MOTOR_RIGHT_DIR_BACKWARD, _dir);
        analogWrite(PIN_MOTOR_RIGHT_PWM_BACKWARD, ~_PWM);
    }
    return true;
}
bool Ardumona::drive_motor_right_with_offset(unsigned int _dir, unsigned char _PWM){
    driver.right_dir = _dir;
    driver.right_pwm = _PWM;
            
    right_motor_flag = _dir;
    if(_dir == MOV_FORWARD){           
        digitalWrite(PIN_MOTOR_RIGHT_DIR_FORWARD, _dir+1);
        analogWrite(PIN_MOTOR_RIGHT_PWM_FORWARD, ~min(max(_PWM + MOTOR_PWM_OFFSET_FORWARD, MOTOR_MIN_PWM), MOTOR_MAX_PWM));
    }else{        
        digitalWrite(PIN_MOTOR_RIGHT_DIR_BACKWARD, _dir);
        analogWrite(PIN_MOTOR_RIGHT_PWM_BACKWARD, ~min(max(_PWM + MOTOR_PWM_OFFSET_BACKWARD, MOTOR_MIN_PWM), MOTOR_MAX_PWM));
    }
    return true;
}
bool Ardumona::drive_motor(m_signal _driver){
    bool drive_flag = true;
    drive_flag &= this->drive_motor_left(_driver.left_dir, _driver.left_pwm);
    drive_flag &= this->drive_motor_right(_driver.right_dir, _driver.right_pwm);
    return drive_flag;
}
bool Ardumona::drive_motor_with_offset(m_signal _driver){
    bool drive_flag = true;
    drive_flag &= this->drive_motor_left_with_offset(_driver.left_dir, _driver.left_pwm);
    drive_flag &= this->drive_motor_right_with_offset(_driver.right_dir, _driver.right_pwm);
    return drive_flag;
}
m_signal Ardumona::convert_wheel_to_signal(m_wheel _in){
    m_signal out;

    if(_in.left_speed >= 0){
        out.left_dir = MOV_FORWARD;    
    }
    else{
        out.left_dir = MOV_BACKWARD;
    }

    if(_in.right_speed >= 0){
        out.right_dir = MOV_FORWARD;        
    }
    else{
        out.right_dir = MOV_BACKWARD;
    }

    out.left_pwm = abs(_in.left_speed);
    out.right_pwm = abs(_in.right_speed);

    return out;
}


/**
* ENCODER COMMAND ROUTINES
* Remark:
* 1. get_{left,right}_encoder_ticks(void)
*	 -Usage: obtaining the raw encoder ticks
* 2. get_{left,right}_time_period(void)
*	 -Usage: obtaining the time period of the encoder sampling
* 3. get_{left,right}_odometry(void)
*	 -Usage: obtaining the odometry data
* 4. get_{left,right}_motor_speed(void)
*	 -Usage: obtaining the motor speed
*/
bool Ardumona::enable_encoder(void){
    left_encoder_counter = 0;
    left_encoder_ticks = 0;
    left_encoder_time_start = 0;
    left_encoder_time_delta = 0;
    right_encoder_counter = 0;
    right_encoder_ticks = 0;
    right_encoder_time_start = 0;
    right_encoder_time_delta = 0;

    attachInterrupt(digitalPinToInterrupt(PIN_LEFT_ENCODER), callback_left_encoder_interrupt, HIGH);
    attachInterrupt(digitalPinToInterrupt(PIN_RIGHT_ENCODER), callback_right_encoder_interrupt, HIGH);
    Serial.println("Encoder OK");
    return true;
}
bool Ardumona::reset_encoder_ticks(bool _left_ticks, bool _right_ticks){
    if(_left_ticks){
        left_encoder_ticks = 0;
    }
    if(_right_ticks){
        right_encoder_ticks = 0;
    }
    return true;
}

void callback_left_encoder_interrupt(void){
    char ch_send[100];
    long tick_per_sec;
    long rev_per_sec;

    left_encoder_counter++;
    left_encoder_ticks++;
    if(left_encoder_counter == 1){
        left_encoder_time_start = micros();
    }
    if(left_encoder_counter >= 101){
        left_encoder_time_delta = micros() - left_encoder_time_start;

        tick_per_sec = 100000000/left_encoder_time_delta;        
        rev_per_sec = tick_per_sec/ENCODER_TICK_PER_REVOLUTION;

        motor.left_speed = 2*CONST_PI*rev_per_sec;        

        left_encoder_counter = 0;        
    }
    if(driver.left_pwm == 0){
        motor.left_speed = 0;
    }
    if(driver.left_pwm > 0){
        if(left_motor_flag == MOV_FORWARD){
            left_odometry_ticks++;
        }
        if(left_motor_flag == MOV_BACKWARD){
            left_odometry_ticks--;
        }
    }
}
long Ardumona::get_left_encoder_ticks(void){
    return left_encoder_ticks;
}
double Ardumona::get_left_time_period(void){
    return pulseIn(PIN_LEFT_ENCODER, HIGH);
}
double Ardumona::get_left_odometry(void){
    return left_odometry_ticks;
}
long Ardumona::get_left_motor_speed(void){
    long output;
    output = motor.left_speed;
    if(driver.left_pwm == 0){
        output = 0;
    }
    if(driver.left_dir == MOV_BACKWARD){
        output = -output;
    }

    return output;
}

void callback_right_encoder_interrupt(void){    
    long tick_per_sec;
    long rev_per_sec;

    right_encoder_counter++;
    right_encoder_ticks++;
    if(right_encoder_counter == 1){
        right_encoder_time_start = micros();
    }
    if(right_encoder_counter >= 101){
        right_encoder_time_delta = micros() - right_encoder_time_start;

        tick_per_sec = 100000000/right_encoder_time_delta;        
        rev_per_sec = tick_per_sec/ENCODER_TICK_PER_REVOLUTION;        

        motor.right_speed = 2*CONST_PI*rev_per_sec;        

        right_encoder_counter = 0;
    }
    if(driver.right_pwm == 0){
        motor.right_speed = 0;
    }
    if(driver.right_pwm > 0){
        if(right_motor_flag == MOV_FORWARD){
            right_odometry_ticks++;
        }
        if(right_motor_flag == MOV_BACKWARD){
            right_odometry_ticks--;
        }
    }
}
long Ardumona::get_right_encoder_ticks(void){
    return right_encoder_ticks;
}
double Ardumona::get_right_time_period(void){
    return pulseIn(PIN_RIGHT_ENCODER, HIGH);
}
double Ardumona::get_right_odometry(void){
    return right_odometry_ticks;
}
long Ardumona::get_right_motor_speed(void){
    long output;
    output = motor.right_speed;
    if(driver.right_pwm == 0){
        output = 0;
    }
    if(driver.right_dir == MOV_BACKWARD){
        output = -output;
    }

    return output;
}


/**
* INFRARED COMMAND ROUTINES
* Remark:
* 1. get_IR_data(void)
*	 -Usage: obtaining all of the IR sensor data
*/
bool Ardumona::enable_IR(void){
    pinMode(PIN_IR_ENABLE, OUTPUT);
    digitalWrite(PIN_IR_ENABLE, HIGH);
    Serial.println("Infrared OK");
    return true;
}
bool Ardumona::disable_IR(void){
    pinMode(PIN_IR_ENABLE, OUTPUT);
    digitalWrite(PIN_IR_ENABLE, LOW);
    return true;
}
m_IR_sensor Ardumona::get_IR_data(void){
    m_IR_sensor out;
    out.right = analogRead(PIN_IR_SIGNAL_RIGHT);
    out.front_right = analogRead(PIN_IR_SIGNAL_FRONTRIGHT);
    out.front = analogRead(PIN_IR_SIGNAL_FRONT);
    out.front_left = analogRead(PIN_IR_SIGNAL_FRONTLEFT);
    out.left = analogRead(PIN_IR_SIGNAL_LEFT);
    return out;
}

/**
* CONTROL COMMAND ROUTINES
* Remark:
* 1. update_desired_control_input(void)
*	 -Usage: assigning the serial-received control commands to the desired control inputs
* 2. update_desired_motor_speed(void)
* 	 -Usage: applying the desired control inputs to the differential drive dynamics	
* 3. update_control_motor_pwm(long, long)
*	 -Usage: applying the desired speed of each motor to the low level controller
*	 -Input: 
*		::desired speed of left motor
*		::desired speed of right motor
* 4. update_controller(void)
*	 -Usage: combining the sequences of controller from serial inputs to applying the command to motors
* 5. update_direct_motor_controller(void)
*	 -Usage: this routine is only for testing the low-level controller, directly assign the serial input
*			 to the desired speed of each motor
* 6. update_PID(m_PID_param, m_PID_signal)
*	 -Usage: it is a general function to calculate the PID control signal for a given input.
*	 -Input: 
*		::struct of controller gains (kp, ki, kd)
*		::struct of previous signals
*/
bool Ardumona::update_desired_control_input(void){
    command_from_pi.linear_velo = min(max(command_from_pi.linear_velo, PHYS_MIN_LINEAR_VELO), PHYS_MAX_LINEAR_VELO);
    command_from_pi.angular_velo = min(max(command_from_pi.angular_velo, PHYS_MIN_ANGULAR_VELO), PHYS_MAX_ANGULAR_VELO);
    this->unicycle_cmd.linear_velo = command_from_pi.linear_velo; // cm * 100
    this->unicycle_cmd.angular_velo = command_from_pi.angular_velo; // rad/s * 100   
    return true;
}
bool Ardumona::update_desired_motor_speed(void){
    this->desired_cmd.left_speed = (2*this->unicycle_cmd.linear_velo - this->unicycle_cmd.angular_velo*PHYS_DIST_WHEEL2WHEEL)/(2*PHYS_VELO_DIVIDER*PHYS_RADIUS_WHEEL);
    this->desired_cmd.right_speed = (2*this->unicycle_cmd.linear_velo + this->unicycle_cmd.angular_velo*PHYS_DIST_WHEEL2WHEEL)/(2*PHYS_VELO_DIVIDER*PHYS_RADIUS_WHEEL);
    
    this->desired_cmd.left_speed = min(max(this->desired_cmd.left_speed, MOTOR_MIN_SPEED), MOTOR_MAX_SPEED);
    this->desired_cmd.right_speed = min(max(this->desired_cmd.right_speed, MOTOR_MIN_SPEED), MOTOR_MAX_SPEED);

    #ifdef LOW_CONTROL_TEST
        Serial.println(this->desired_cmd.left_speed);
        Serial.println(this->desired_cmd.right_speed);
    #endif

    return true;
}
bool Ardumona::update_control_motor_pwm_pid(long _des_left_speed, long _des_right_speed){
    long dt;
    m_signal control_signal;
    m_wheel assigned_pwm;

    this->motor_pid.kp = 250; //150, 350
    this->motor_pid.ki = 3; //15
    this->motor_pid.kd = 0;
    this->motor_pid.divider = 10000; // 1000
    
    dt = micros() - this->control_start_time;

    this->left_motor_signal.dt = dt;
    this->left_motor_signal.desired_value = _des_left_speed;    
    this->left_motor_signal.actual_value = this->get_left_motor_speed();
    
    this->right_motor_signal.dt = dt;
    this->right_motor_signal.desired_value = _des_right_speed;
    this->right_motor_signal.actual_value = this->get_right_motor_speed();

    this->left_motor_signal = this->update_PID(this->motor_pid, this->left_motor_signal);
    this->right_motor_signal = this->update_PID(this->motor_pid, this->right_motor_signal);

    assigned_pwm.left_speed = this->left_motor_signal.control_input;
    assigned_pwm.right_speed = this->right_motor_signal.control_input;

    control_signal = this->convert_wheel_to_signal(assigned_pwm);

    #ifdef LOW_CONTROL_TEST
        float m_sec = micros();
        char word[100];
        sprintf(word, "Data>> ");
        sprintf(word, "%s# Motor:%d", word, this->left_motor_signal.actual_value);
        sprintf(word, "%s#%d", word, this->right_motor_signal.actual_value);
        sprintf(word, "%s# Error:%d", word, this->left_motor_signal.previous_error);
        sprintf(word, "%s#%d", word, this->right_motor_signal.previous_error);    
        sprintf(word, "%s# PWM:%d", word, assigned_pwm.left_speed);
        sprintf(word, "%s#%d", word, assigned_pwm.right_speed);
        sprintf(word, "%s# Control:%d", word, control_signal.left_pwm);
        sprintf(word, "%s#%d", word, control_signal.right_pwm);
        sprintf(word, "%s# Driver:%d", word, driver.left_pwm);
        sprintf(word, "%s#%d", word, driver.right_pwm);
        Serial.println(word);
        m_sec = micros() - m_sec;
        Serial.println(m_sec);
    #endif    
    delayMicroseconds(25000);
    
    this->control_start_time = micros();    
    return this->drive_motor_with_offset(control_signal);
}
bool Ardumona::update_controller(void){
    bool ctrl_flag = true;
    ctrl_flag &= this->update_desired_control_input();
    ctrl_flag &= this->update_desired_motor_speed();
    ctrl_flag &= this->update_control_motor_pwm_pid(this->desired_cmd.left_speed, this->desired_cmd.right_speed);
    return ctrl_flag;
}
bool Ardumona::update_direct_motor_controller(void){
    bool ctrl_flag = true;
    this->desired_cmd.left_speed = command_from_pi.linear_velo; // Left speed
    this->desired_cmd.right_speed = command_from_pi.angular_velo; // Right speed

    ctrl_flag &= this->update_control_motor_pwm_pid(this->desired_cmd.left_speed, this->desired_cmd.right_speed);
    return ctrl_flag; // for this function, the input command_from_pi.linear_velo is the linear speed of motor left,
                    // command_from_pi.angular_velo, the input desireded linear speed of motor right 
}
m_PID_signal Ardumona::update_PID(m_PID_param _gain, m_PID_signal _input){
    long current_error;
    long derivative_error;
    long sum_error;
    long pid_command;

    m_PID_signal output;

    current_error = _input.desired_value - _input.actual_value;
    derivative_error = 1000000*(current_error - _input.previous_error)/_input.dt;
    sum_error = _input.sum_error + (current_error*_input.dt)/1000000;

    pid_command = _gain.kp*current_error + _gain.ki*sum_error + _gain.kd*derivative_error;
    output.control_input = _input.control_input + pid_command/_gain.divider;
    output.previous_error = current_error;
    output.sum_error = sum_error;
    return output;
}
